﻿#include "precompiled.h"
#include "theGame.h"
#include <string>



void theGame::SetConsoleWindow()
{
	AllocConsole();

	HWND console = GetConsoleWindow();
	RECT ConsoleRect;
	GetWindowRect(console, &ConsoleRect);
	MoveWindow(console, ConsoleRect.left, ConsoleRect.top-25, 500, 350, TRUE);

	HANDLE consoleOutputHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTitle(TEXT("FAKKEN SNAEK"));
	SyncWindows();
	
	CONSOLE_CURSOR_INFO cci;
	cci.bVisible = false;
	cci.dwSize = 1;

	CONSOLE_SCREEN_BUFFER_INFO csbi;
	for (auto i = 0; i < _amountOfScreenBuffers; i++)
	{
		this->hConsoleScreenBuffers[i] = CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CONSOLE_TEXTMODE_BUFFER, NULL);
		SetConsoleTextAttribute(hConsoleScreenBuffers[i], 0x0a);
		SetConsoleCursorInfo(hConsoleScreenBuffers[i], &cci);
	}

	SetConsoleActiveScreenBuffer(hConsoleScreenBuffers[_currentScreenBufferNumber]);
	CloseHandle(consoleOutputHandle);
}

void theGame::SwapCSB()
{
	this->_currentScreenBufferNumber = (_currentScreenBufferNumber + 1) % _amountOfScreenBuffers;
		SetConsoleActiveScreenBuffer(this->hConsoleScreenBuffers[_currentScreenBufferNumber]);
}

void theGame::update_score(UINT plus) const
{
	this->score += plus;
}

void theGame::SyncWindows()
{
	HWND consoleWindow = GetConsoleWindow();
	LONG style = GetWindowLong(consoleWindow, GWL_STYLE);
	style = style & ~(WS_MINIMIZEBOX|WS_MAXIMIZEBOX|WS_HSCROLL|WS_VSCROLL|WS_SIZEBOX);
	SetWindowLong(consoleWindow, GWL_STYLE, style);
}

theGame::~theGame()
{

	for (auto i = 0; i < _amountOfScreenBuffers; i++)
		CloseHandle(this->hConsoleScreenBuffers[i]);

	delete[] this->hConsoleScreenBuffers;

	CloseHandle(this->hThread_GameLoop);

	CloseHandle(this->hMutex_GameLoop);
	
}

const bool &theGame::OnAir()
{
	return this->onAir;

}

const int & theGame::get_score()
{
	return this->score;
}

theGame::theGame(int n, DWORD _mainThreadID)
{
	this->hConsoleScreenBuffers = new void*[n];
	this->_currentScreenBufferNumber = 0;
	this->_amountOfScreenBuffers = n;
	this->mainThreadID = _mainThreadID;
}

