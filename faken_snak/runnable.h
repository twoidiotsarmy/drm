
class runnable
{
public:
	virtual ~runnable() {}

	static unsigned long __stdcall run_thread (void* args)
	{
		runnable *prunnable = static_cast<runnable*>(args);
		return prunnable->run();
	}
protected:
	virtual unsigned long run() = 0; //derived class must implement this!
};