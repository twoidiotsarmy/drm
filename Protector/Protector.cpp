// Protector.cpp: ���������� ���������������� ������� ��� ���������� DLL.
//
#include "stdafx.h"
#include "Protector.h"

#define x86

using namespace CryptoPP;
using namespace std;

extern "C"
{
	//THIS MUST BE OBFUSCATED
#ifdef x86
	__declspec(dllexport) DWORD Vrf(DWORD OEP)
	{
		SYSTEMTIME st; DWORD jumpPoint;
		GetSystemTime(&st);
		bool inOverlay = true;

		DWORD IMAGE_BASE = (DWORD)GetModuleHandleA(NULL);
		IMAGE_DOS_HEADER *IDH = (IMAGE_DOS_HEADER*)IMAGE_BASE;
		IMAGE_NT_HEADERS32 *INHs = (IMAGE_NT_HEADERS32 *)(IMAGE_BASE + IDH->e_lfanew);
		IMAGE_SECTION_HEADER* sections = (IMAGE_SECTION_HEADER*)((DWORD)INHs + sizeof(*INHs));

		if ((char*)sections[INHs->FileHeader.NumberOfSections - 1].Name == ".prtctr")
			inOverlay = false;

		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> dis(0x00BF0000,0xFC144A0D);



		if (innerUnlocker(OEP))
		{
			
		}
		else {
			jumpPoint= (DWORD)dis(gen);
			OEP = jumpPoint;
		}
		return jumpPoint/2;
	}

#else
	__declspec(dllexport) DWORD64 Vrf(DWORD64 *OEP)
	{
		SYSTEMTIME st;
		GetSystemTime(&st);


		HINSTANCE IMAGE_BASE = GetModuleHandleA(NULL);

		std::string title = "YOU DID IT ONCE [" + std::to_string(st.wHour) + ":" + std::to_string(st.wMinute) + ":" + std::to_string(st.wSecond) + "]";
		MessageBoxA(NULL, "VERIFY INVOKED\r\n", title.c_str(), MB_OK);


		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> dis(0x00BF0000, 0xFC144A0D);

		return (DWORD64)OEP + (DWORD64)IMAGE_BASE;
	}
#endif

}

bool innerUnlocker(DWORD OEP) 
{
	bool inOverlay = true;

	DWORD IMAGE_BASE = (DWORD)GetModuleHandleA(NULL);
	IMAGE_DOS_HEADER *IDH = (IMAGE_DOS_HEADER*)IMAGE_BASE;
	IMAGE_NT_HEADERS32 *INHs = (IMAGE_NT_HEADERS32 *)(IMAGE_BASE + IDH->e_lfanew);
	IMAGE_SECTION_HEADER* sections = (IMAGE_SECTION_HEADER*)((DWORD)INHs + sizeof(*INHs));

	//hold on. OEP should be encrypted as well!
	OEP = OEP + IMAGE_BASE;
	for (int i = 0; i < INHs->FileHeader.NumberOfSections - 1; i++)
		if ((sections[i].Characteristics&IMAGE_SCN_MEM_EXECUTE)>0)
		{
			DWORD oldProtect = 0, dummy = 0, SIZE = sections[i].Misc.VirtualSize;
			if (inOverlay)
				SIZE -= X_CODEx86_SIZE;
			if (VirtualProtect((DWORD*)(IMAGE_BASE + sections[i].VirtualAddress), SIZE, PAGE_READWRITE, &oldProtect) == 0)
				MBOX(("ERROR: " + std::to_string(GetLastError())).c_str());

			CryptoAlgorithm Salsa;//salsa needs a key!
			CryptoAlgorithm CA = CaesarCypher;
			DWORD caesarKey = 5;


			Decrypt((BYTE*)(IMAGE_BASE + sections[i].VirtualAddress), SIZE, CaesarCypher, &caesarKey);
			if (VirtualProtect((void*)(IMAGE_BASE + sections[i].VirtualAddress), SIZE, oldProtect, &dummy) == 0)
				MBOX(("ERROR 2: " + std::to_string(GetLastError())).c_str());
		}
	//by this time you must have decrypted the shit out of image. time to fix relocations
	fixRelocations();
	//if (REG_KEY_EXISTS(HKEY_LOCAL_MACHINE,L"Software\\Protector\\"+GetModuleFileNameW(),))
	return true;
}

bool Decrypt(BYTE* target, DWORD size, CryptoAlgorithm CA, void* parameters)
{
	CA(target, size, parameters);
	return true;
}


void CaesarCypher(BYTE* pointer, DWORD size, void* param)
{
	DWORD key = *(DWORD*)param;
	for (int i = 0; i < size; i++)
		pointer[i] = pointer[i] - key;
}

void Salsa_Decrypt(BYTE* pointer, DWORD size, void* param)
{
	SalsaBundle *KeyBundle = (SalsaBundle*)param;
	AutoSeededRandomPool prng;

	byte *plaintextBytes = new byte[size];
	byte *ciphertextBytes = (byte*)pointer;//(byte*)ciphertextStr.c_str(); 

	byte key[32];
	byte iv[8];
	prng.GenerateBlock(key, 32);
	prng.GenerateBlock(iv, 8);

	Salsa20::Encryption salsa;
	salsa.SetKeyWithIV(key, 32, iv);
	salsa.ProcessData(plaintextBytes, ciphertextBytes, size);

	for (auto i = 0; i < size; i++)
		pointer[i] = plaintextBytes[i];
}

bool REG_KEY_EXISTS(HKEY hKey, LPCWSTR lpSubKey, LPCTSTR lpValueName, DWORD KeyValue)
{
	HKEY rKey;
	return RegOpenKeyEx(hKey, lpSubKey, 0, KEY_ALL_ACCESS, &rKey) == ERROR_SUCCESS;
}

bool REG_ADD(HKEY hKey, LPCWSTR lpSubKey, LPCTSTR lpValueName, DWORD KeyValue)
{
	HKEY rKey;
	DWORD dwDisposition;
	LONG RegStatus;
	TCHAR msg[512];

	if (RegOpenKeyEx(hKey, lpSubKey, 0, KEY_ALL_ACCESS, &rKey) != ERROR_SUCCESS)
	{
		rKey = hKey;
		RegCreateKeyEx(rKey, lpSubKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &rKey, &dwDisposition);
		if (dwDisposition != REG_CREATED_NEW_KEY)
		{
			FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, GetLastError(), 0, &msg[0], 512, 0);
			MessageBox(0, &msg[0], L"RegCreateKeyEx_Error", MB_OK | MB_ICONEXCLAMATION);
			RegCloseKey(rKey);
			return false;
		}
	}

	if (RegSetValueEx(rKey, lpValueName, 0, REG_DWORD, (const BYTE*)&KeyValue, sizeof(KeyValue)) == ERROR_SUCCESS)
	{
		RegCloseKey(rKey);
		return true;
	}
	else
	{
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, GetLastError(), 0, &msg[0], 512, 0);
		MessageBox(0, &msg[0], L"RegSetValueEx_Error", MB_OK | MB_ICONEXCLAMATION);
		RegCloseKey(rKey);
		return false;
	}
}


//consider calling VirtualProtect only 2 times on the whole area if it's possible
void fixRelocations()
{
	DWORD REAL_IMAGE_BASE = (DWORD)GetModuleHandleA(NULL);
	DWORD PREFERRED_IMAGE_BASE;
	DWORD RelocStart;
	DWORD CurrentRelocPageRVA;
	DWORD BlockSize;
	DWORD TypeOffset;
	WORD* FixupIterator;
	DWORD oldProtect;
#define fixupType(x) x>>12
#define fixupOffset(x) x&0x0FFF
#define delta REAL_IMAGE_BASE-PREFERRED_IMAGE_BASE

	IMAGE_DOS_HEADER *IDH = (IMAGE_DOS_HEADER*)REAL_IMAGE_BASE;
	IMAGE_NT_HEADERS32 *INHs = (IMAGE_NT_HEADERS32 *)(REAL_IMAGE_BASE + IDH->e_lfanew);
	PREFERRED_IMAGE_BASE = INHs->OptionalHeader.ImageBase;
	if (!delta)
		return;
	IMAGE_SECTION_HEADER* sections = (IMAGE_SECTION_HEADER*)((DWORD)INHs + sizeof(*INHs));

	//�� ��� ��� ���� �������� � ����� ������... ��...
	for (int i = 0; i < INHs->FileHeader.NumberOfSections; i++)
		if ((char*)sections[i].Name == ".reloc")
		{
			RelocStart = REAL_IMAGE_BASE + sections[i].VirtualAddress;
			break;
		}

	if (!RelocStart)
		return;
	IMAGE_BASE_RELOCATION* relocs = (IMAGE_BASE_RELOCATION*)RelocStart;
	DWORD CurrentAddress;

	//TODO: oh wait i'm not sure
	__asm {
		CALL $ + 5
		POP [CurrentAddress]
	}

	//opening all sections for writing
	DWORD *oldProtections = new DWORD[INHs->FileHeader.NumberOfSections];
	for (auto i = 0;i<INHs->FileHeader.NumberOfSections;i++)
		if (CurrentAddress>REAL_IMAGE_BASE + sections[i].VirtualAddress && CurrentAddress < REAL_IMAGE_BASE + sections[i].VirtualAddress+sections[i].Misc.VirtualSize)
			VirtualProtect((void*)(REAL_IMAGE_BASE+sections[i].VirtualAddress), sections[i].Misc.VirtualSize, PAGE_EXECUTE_READWRITE, &oldProtections[i]);
		else VirtualProtect((void*)(REAL_IMAGE_BASE + sections[i].VirtualAddress), sections[i].Misc.VirtualSize, PAGE_READWRITE, &oldProtections[i]);

	do
	{
		CurrentRelocPageRVA = REAL_IMAGE_BASE+relocs->VirtualAddress;
		BlockSize = relocs->SizeOfBlock-(sizeof(relocs->SizeOfBlock) + sizeof(relocs->VirtualAddress));
		FixupIterator = (WORD*)(relocs + sizeof(relocs->SizeOfBlock) + sizeof(relocs->VirtualAddress));

		VirtualProtect((DWORD*)(RelocStart + CurrentRelocPageRVA), 0x1000, PAGE_READWRITE, &oldProtect);

		for (int i = 0; i < BlockSize; i += 2)
		{
			switch (fixupType(*FixupIterator))
			{
				case IMAGE_REL_BASED_ABSOLUTE:
				case IMAGE_REL_BASED_RESERVED:
				case IMAGE_REL_BASED_MACHINE_SPECIFIC_7:
				default:
					break;
				case IMAGE_REL_BASED_HIGH:
					*((WORD*)(REAL_IMAGE_BASE + CurrentRelocPageRVA + fixupOffset(*FixupIterator))) += (WORD)(delta & 0xFFFF0000);
					break;
				case IMAGE_REL_BASED_LOW:
					*((WORD*)(REAL_IMAGE_BASE + CurrentRelocPageRVA + fixupOffset(*FixupIterator))) += (WORD)(delta & 0x0000FFFF);
					break;
				case IMAGE_REL_BASED_HIGHLOW:
					*((DWORD*)(REAL_IMAGE_BASE + CurrentRelocPageRVA + fixupOffset(*FixupIterator))) += delta;
					break;
				case IMAGE_REL_BASED_HIGHADJ:
					*((WORD*)(REAL_IMAGE_BASE + CurrentRelocPageRVA + fixupOffset(*FixupIterator))) += (WORD)(delta & 0xFFFF0000);
					*((DWORD*)(REAL_IMAGE_BASE + CurrentRelocPageRVA + fixupOffset(*FixupIterator))) |= (DWORD)(*FixupIterator & 0x0000FFFF);
					break;
				case IMAGE_REL_BASED_DIR64:
					*((DWORD64*)(REAL_IMAGE_BASE + CurrentRelocPageRVA + fixupOffset(*FixupIterator))) += delta;
					break;
			}
		}
		//am not sure, i suppose block size might be divisible only by 2 but not 4 (although i recall that each fixup block is aligned by 32 bits 
		relocs += BlockSize/sizeof(BlockSize);
		//VirtualProtect((DWORD*)(RelocStart + CurrentRelocPageRVA), 0x1000, oldProtect, &oldProtect);

	} while ((DWORD)relocs < REAL_IMAGE_BASE + RelocStart + INHs->OptionalHeader.DataDirectory[5].Size);

	for (auto i = 0; i<INHs->FileHeader.NumberOfSections; i++)
		VirtualProtect((void*)(REAL_IMAGE_BASE + sections[i].VirtualAddress), sections[i].Misc.VirtualSize, oldProtections[i], &oldProtect);


}
#undef delta
#undef fixupType(x)
#undef fixupOffset(x)

void func()
{
}