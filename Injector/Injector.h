#include "precompiled.h"
#include "PE_Manager.h"
//#include "OpenFileDialog.h"

#pragma region defines
//this one gets substituted with OEP (RVA) of selected executable in the x code
//0xD0C0ADDE
//0xDEADC0D0
#define OEP (DWORD)0xDEADC0D0
//this one gets substituted with Kernel32::LoadLibraryA RVA of the executable in the x code
//0xD1C0ADDE
#define LOAD_LIBRARY_A_ADDRESS (DWORD)0xDEADC0D1

//this one gets substituted with Kernel32::GetProcAddress RVA of the executable in the x code
//0xD2C0ADDE
#define GET_PROC_ADDRESS_ADDRESS (DWORD)0xDEADC0D2 

//this one gets substituted with Kernel32::FreeLibrary RVA of the executable in the x code
//0xD3C0ADDE
#define FREE_LIBRARY_ADDRESS (DWORD)0xDEADC0D3 

#pragma endregion


//typedef std::string&(*CryptoAlgorithm)(std::string& s, void*);
class Injector:public PE_Manager
{
public:
	/// <summary>
	/// opens ifstream, gathers info about EP, address of LoadLibrary, GetProcAddress, notifies if any of those are missing
	/// </summary>
	/// <param name="filename">target executable file name</param>

	//Specify path to a binary file with OP-codes to inject
	bool SelectInjection();

	//Select a PE file
	bool SelectExecutable();
	/// <summary>
	/// injects specified x-code into executable
	/// </summary>
	/// <param name="sectionTail">append x-code to section tail if possible. if not - new section is created</param>
	void Inject(bool sectionTail = false);

	void AddImport(std::unordered_map<std::string, std::vector<std::string>>* importData);

	//bool Encrypt(CryptoAlgorithm CA, DWORD key);
	inline const bool& InOverlay() { return this->inOverlay; }
	inline const unsigned int& Xcode_size() { return this->xcode_size; }
	//substitutes all keys from SubstitutionMap in x-code with relevant values
	bool SubstitutePatterns();

	/// <summary>
	/// scans executable static import library (DIRECTORY ENTRY 12) for specified functions in a specified library. maps them with SubstitutionMap if finds
	/// </summary>
	/// <param name="dllName">obvious</param>
	/// <param name="functionNames">obvious</param>
	/// <returns>true if filled substitution map successfully, false if didn't manage to find all required imported functions</returns>
	bool mapRequiredFunctions(char* dllName, std::unordered_map<std::string, DWORD>functionNameMap);
	
private:
	unsigned int xcode_size;
	bool inOverlay;
	bool canAppentOverlayTo(pe_bliss::section&);
	//specifies if everything is fine

	std::string x_code;
	std::unordered_map<DWORD,DWORD> SubstitutionMap;

};


//namespace Algorithms
//{
//	std::string& CaesarCypher(std::string& s, void* key);
//	std::string& AES(std::string& s, void* key);
//	std::string& Salsa20(std::string&, void*);
//}