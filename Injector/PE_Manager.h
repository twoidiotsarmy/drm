#include "precompiled.h"
#pragma once

#include "OpenFileDialog.h"
#include "SaveFileDialog.h"

class PE_Manager {
public:
	virtual bool SelectExecutable();


	PE_Manager();
	~PE_Manager();
	// Creates a new portable executable file next to original with all commited changes
	void RebuildExecutable();
	//inline pe_bliss::pe_base* shareExecutable() { return this->executable; }
	void receiveExecutable(pe_bliss::pe_base* outerExe);
	void borrowExecutable(PE_Manager&);
	inline const bool& OnAir() { return this->onAir; }
protected:
	pe_bliss::pe_base *executable;
	wchar_t* currentFilename;
	bool isValidPE();
	bool onAir;
};
