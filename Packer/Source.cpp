#include <Windows.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <conio.h>
#include <string>

using namespace std;
void getConsole();
void Quit();

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow) 
{
	getConsole();
	cout << "We are on air" << endl;
	string s;
	getline(cin, s);
	cout << s << endl;

	_getch();
	Quit();
}

void Quit()
{
	FreeConsole();
	exit(0);
}
void getConsole()
{
	AllocConsole();
	setlocale(LC_ALL, "Russian");

	FILE * pNewStdout = nullptr;
	FILE * pNewStderr = nullptr;
	FILE * pNewStdin = nullptr;

	::freopen_s(&pNewStdout, "CONOUT$", "w", stdout);
	::freopen_s(&pNewStderr, "CONOUT$", "w", stderr);
	::freopen_s(&pNewStdin, "CONIN$", "r", stdin);

	// Clear the error state for all of the C++ standard streams. Attempting to accessing the streams before they refer
	// to a valid target causes the stream to enter an error state. Clearing the error state will fix this problem,
	// which seems to occur in newer version of Visual Studio even when the console has not been read from or written
	// to yet.
	std::cout.clear();
	std::cerr.clear();
	std::cin.clear();

	std::wcout.clear();
	std::wcerr.clear();
	std::wcin.clear();
}