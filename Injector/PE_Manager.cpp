#pragma once
#include "precompiled.h"
#include "PE_Manager.h"


using namespace std;
using namespace pe_bliss;

bool PE_Manager::SelectExecutable()
{
	OpenFileDialog* OFD = new OpenFileDialog();
	OFD->Filter = _T("exe files (*.exe)\0*.exe\0");
	OFD->Flags |= OFN_SHOWHELP | OFN_EXPLORER;
	OFD->InitialDir = _T("S:\\Games\\ppsspp");
	//OFD->InitialDir = _T("S:\\");
	OFD->Title = _T("Select an executable");

	if (!OFD->ShowDialog())
	{
		cout << "selecting an executable canceled" << endl;
		return false;
	}

	this->currentFilename = OFD->FileName;
	ifstream exeStream(OFD->FileName, ios::in | ios::binary);
	if (!exeStream.is_open())
	{
		cout << "can't open stream. maybe file is corrupted and\or is occupied by an another program" << endl;
		return false;
	}
	//pe_base executable = pe_factory::create_pe(exeStream, OFD->FileName);
	this->executable = new pe_base((pe_factory::create_pe(exeStream, OFD->FileName)));

	if (!this->isValidPE())
	{
		cout << "some weird shit is happening on sesame street: selected file isn't a valid PE file" << endl;
		return false;
	}
	this->onAir = true;
	delete OFD;
	return true;
}
PE_Manager::PE_Manager()
{
	this->onAir = false;
}
PE_Manager::~PE_Manager()
{
	if (this->executable != nullptr)
	{
		delete this->executable;
		this->executable = nullptr;
	}
	if (this->currentFilename!=nullptr)
		delete this->currentFilename;
}

void PE_Manager::RebuildExecutable()
{
	//IMAGE_FILE_RELOCS_STRIPPED | IMAGE_FILE_EXECUTABLE_IMAGE | IMAGE_FILE_LARGE_ADDRESS_AWARE | IMAGE_FILE_32BIT_MACHINE equals to 0x123
	WORD newCharacteristics = IMAGE_FILE_RELOCS_STRIPPED | IMAGE_FILE_EXECUTABLE_IMAGE | IMAGE_FILE_LARGE_ADDRESS_AWARE | IMAGE_FILE_32BIT_MACHINE;
	//this->executable->set_characteristics(newCharacteristics);

	//disgusting
	//this->executable->set_characteristics(newCharacteristics);
	//this->executable->set_directory_rva(IMAGE_DIRECTORY_ENTRY_BASERELOC, 0);
	this->executable->set_directory_size(IMAGE_DIRECTORY_ENTRY_BASERELOC, 0);

	SaveFileDialog SFD;
	wstring destination(this->currentFilename);
	destination = destination.substr(0, destination.find_last_of(L"/\\"));

	SFD.DefaultExtension = TEXT(".exe");
	SFD.Filter = _T("exe files (*.exe)\0*.exe\0");
	SFD.Flags |= OFN_SHOWHELP | OFN_EXPLORER;
	SFD.InitialDir = (TCHAR*)destination.data();
	SFD.Title = _T("Set destination for patched executable");
	
	if (SFD.ShowDialog())
	{
		std::ofstream new_pe_file(SFD.FileName, std::ios::out | std::ios::binary | std::ios::trunc);

		if (!new_pe_file)
		std::wcout << "Cannot create " << SFD.FileName << std::endl;

		rebuild_pe(*this->executable, new_pe_file);

		wcout << L"check it in " << SFD.FileName << endl;
	}
}

void PE_Manager::borrowExecutable(PE_Manager& pem)
{
	this->executable = pem.executable;
}

void PE_Manager::receiveExecutable(pe_bliss::pe_base * outerExe)
{
	this->executable = outerExe; 
	this->currentFilename = (wchar_t*)this->executable->get_filename(); 
}

bool PE_Manager::isValidPE()
{
	pe_type pt = this->executable->get_pe_type();
	return (pt == pe_type_32 || pt == pe_type_64) && this->executable->get_dos_header().e_magic == MZ&&this->executable->get_pe_signature() == PE_SIGNATURE;
}
