#include "precompiled.h"
#include "Injector.h"
#include "Encryptor.h"
using namespace pe_bliss;
using namespace std;

void getConsole();
void Quit();
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{

	getConsole();

	Injector theInjector;
	Cryptor theEncryptor;
	theInjector.SelectExecutable();
	if (!theInjector.OnAir())
	{
		cout << "injector's not on air" << endl;
		Quit();
	}

	if (!theInjector.mapRequiredFunctions ("kernel32.dll",	unordered_map<string, DWORD>
															{
																pair<string, DWORD>(string("LoadLibraryA\0"), LOAD_LIBRARY_A_ADDRESS),
																pair<string, DWORD>(string("GetProcAddress\0"), GET_PROC_ADDRESS_ADDRESS),
																//pair<string, DWORD>(string("FreeLibrary\0"), FREE_LIBRARY_ADDRESS)
															}
		)) Quit();

	if (theInjector.SelectInjection())
		if (theInjector.SubstitutePatterns())
		{
			theInjector.Inject(MessageBoxA(NULL, "Append x code to executable section overlay if possible?", "Settings", MB_YESNO | MB_ICONQUESTION)==IDYES);

			if (MessageBoxA(NULL, "Encrypt executable and readable sections?", "Settings", MB_YESNO|MB_ICONQUESTION) == IDYES)
			{
				theEncryptor.borrowExecutable(theInjector);
				CryptoAlgorithm CA = Algorithms::Salsa20;
				//theInjector.Encrypt(CA, 5);
				theEncryptor.Encrypt(CA, theEncryptor.GenerateKey(), theInjector.InOverlay(), theInjector.Xcode_size());
			}
			theInjector.RebuildExecutable();
		}
	_getch();
	FreeConsole();

	return 0;
}

void Quit()
{
	_getch();
	FreeConsole();
	exit(0);
}

void getConsole()
{
	AllocConsole();
	setlocale(LC_ALL, "Russian");

	freopen("CONOUT$", "w", stdout);
	freopen("CONIN$", "r", stdin);
}