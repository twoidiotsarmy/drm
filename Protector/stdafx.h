// stdafx.h: ���������� ���� ��� ����������� ��������� ���������� ������
// ��� ���������� ������ ��� ����������� �������, ������� ����� ������������, ��
// �� ����� ����������
//

#pragma once
#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // ��������� ����� ������������ ���������� �� ���������� Windows
// ����� ���������� Windows:
#define CRYPTOPP_DEFAULT_NO_DLL
#include <windows.h>

#include "modes.h"
#include "aes.h"

#include "salsa.h"
#include "osrng.h"

#include <string>
#include <time.h>
#include <random>
#include <string>
#include <stddef.h>

// TODO: ���������� ����� ������ �� �������������� ���������, ����������� ��� ���������
