#include "precompiled.h"
#include "modes.h"
#include "salsa.h"
#include "Encryptor.h"

using namespace pe_bliss;
using namespace std;
using namespace CryptoPP;
Cryptor::Cryptor()
{
}

Cryptor::~Cryptor()
{
}

bool Cryptor::Encrypt(CryptoAlgorithm CA, const byte const * key, const bool& inOverlay, const int& padding)
{
	if (!this->executable)
		return false;
	pe_bliss::section_list sections = this->executable->get_image_sections();
	for (auto i = 0; i<sections.size(); i++)
		if ((sections[i].get_characteristics()&IMAGE_SCN_MEM_EXECUTE) > 0)
		{
			if (!inOverlay && sections[i].get_name()!=".prtctr")
				sections[i].get_raw_data() = CA(sections[i].get_raw_data(), key);
			else sections[i].get_raw_data() = CA(sections[i].get_raw_data().substr(0, sections[i].get_virtual_size() - padding), key)
											+ sections[i].get_raw_data().substr(sections[i].get_virtual_size() - padding, padding);
			this->executable->get_image_sections()[i] = sections[i];// = temp;
		}
	return true;
}

const byte * const Cryptor::GenerateKey()
{
	AutoSeededRandomPool prng;
	byte key[32];
	prng.GenerateBlock(key, 32);
	return key;
}


std::string& Algorithms::CaesarCypher(std::string& s, const byte const * params)
{
	DWORD key = *((DWORD*)params);
	for (int i = 0; i < s.size(); i++)
		s[i] = s[i] + key;
	return s;
}

std::string & Algorithms::AES(std::string & s, byte const * const key)
{
	string rezult;
	byte iv[CryptoPP::AES::MAX_KEYLENGTH];
		//key[CryptoPP::AES::MAX_KEYLENGTH], 
	memset((void*)key, 0x00, CryptoPP::AES::MAX_KEYLENGTH);
	SHA256 sha256;
	RandomNumberGenerator RNG;

	byte randomLength = 16 + (RNG.GenerateByte() % 49);
	byte* magic = new byte[randomLength];
	for (auto i = 0; i < randomLength; i++)
		magic[i] = RNG.GenerateByte();

	sha256.CalculateDigest((byte*)key, magic, randomLength);
	memset(iv, 0x00, CryptoPP::AES::BLOCKSIZE);

	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::MAX_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(rezult));
	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(s.c_str()), s.length() + 1);
	stfEncryptor.MessageEnd();

	std::cout << "Cipher Text (" << rezult.size() << " bytes)" << std::endl;

	for (int i = 0; i < rezult.size(); i++)
		std::cout << "0x" << std::hex << (0xFF & static_cast<byte>(rezult[i])) << " ";

	std::cout << std::endl << std::endl;

	//CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	//CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	//CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedtext));
	//stfDecryptor.Put(reinterpret_cast<const unsigned char*>(ciphertext.c_str()), ciphertext.size());
	//stfDecryptor.MessageEnd();

	////
	//// Dump Decrypted Text
	////
	//std::cout << "Decrypted Text: " << std::endl;
	//std::cout << decryptedtext;
	//std::cout << std::endl << std::endl;

	return s;
}

std::string Algorithms::Salsa20(std::string& s, const byte const * key)
{
	AutoSeededRandomPool prng;

	string ciphertextStr("");
	byte *plaintextBytes = (byte*)s.data();
	//I could statically allocate this, but then changes will require work, and typing
	byte *ciphertextBytes = new byte[s.length()];

	//~Key and IV Generation/Initialization======================================
	//byte key[32];
	byte iv[8];
	//prng.GenerateBlock(key, 32);
	prng.GenerateBlock(iv, 8);

	//~Encryption================================================================
	Salsa20::Encryption salsa;
	salsa.SetKeyWithIV(key, 32, iv);
	salsa.ProcessData(ciphertextBytes, plaintextBytes, s.length());
	ciphertextStr.assign((char*)ciphertextBytes);
	cout << "MASTER KEY FOR TEXT SECTION: ";
	for (auto i = 0;i<32;i++)
		cout << ((int)key[i] & 0xFF) << " ";
	cout << endl;

	cout << "IV FOR MASTER KEY: ";
	for (const byte &x : iv)
		cout << ((int)x & 0xFF) << " ";
	cout << endl;

	//Reset Key & IV
	//!!! THIS IS IMPORTANT: If you do not reset the stream cipher the data will
	//be encrypted again with a different part of the streaming key
	//Resetting the key & IV ensure that the same key is used, and we decrypt
	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	return ciphertextStr;
}