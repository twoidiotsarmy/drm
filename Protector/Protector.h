#pragma once
#include "stdafx.h"

#define MBOX(x) MessageBoxA(NULL,x,"INFO",MB_OK)
#define X_CODEx86_SIZE 157

using byte = unsigned char;
struct SalsaBundle
{
	byte key[32];
	byte iv[8];
};

bool innerUnlocker(DWORD OEP);
typedef void(*CryptoAlgorithm)(BYTE*, DWORD, void*);
bool Decrypt(BYTE* target, DWORD size, CryptoAlgorithm CA, void* param);
void CaesarCypher(BYTE* pointer, DWORD size, void* key);
void Salsa_Decrypt(BYTE* pointer, DWORD size, void* param);
//NOT IMPLEMENTED. Salsa is considered to be a high-speed stream cipher, while AES is not

bool REG_KEY_EXISTS(HKEY hKey, LPCWSTR lpSubKey, LPCTSTR lpValueName, DWORD KeyValue);

bool REG_ADD(HKEY hKey, LPCWSTR lpSubKey, LPCTSTR lpValueName, DWORD KeyValue);

void fixRelocations();