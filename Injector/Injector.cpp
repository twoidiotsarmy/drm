#include "precompiled.h"
#include "salsa.h"
#include "Injector.h"

using namespace pe_bliss;
using namespace std;
using namespace CryptoPP;

using CryptoPP::Salsa20;

DWORD ReverseDWORD(DWORD dword)
{
	return (dword & 0x000000ff) << 24 | (dword & 0x0000ff00) << 8 | (dword & 0x00ff0000) >> 8 | (dword & 0xff000000) >> 24;
}

bool Injector::SelectInjection()
{
	OpenFileDialog* OFD = new OpenFileDialog();
	OFD->Filter = _T("bin files (*.bin)\0*.bin\0");
	OFD->Flags |= OFN_SHOWHELP | OFN_EXPLORER;
	OFD->InitialDir = _T("T:\\..");
	OFD->Title = _T("Select a binary file with OP-codes");
	if (!OFD->ShowDialog())
		return false;

	ifstream xcodeStream(OFD->FileName, ios::binary | ios::in| ios::ate);
	if (xcodeStream.is_open())
		this->xcode_size = (UINT32)xcodeStream.tellg();
	xcodeStream.seekg(xcodeStream.beg);
	//this->x_code = new char[this->xcode_size];

	std::stringstream buffer;
	buffer << xcodeStream.rdbuf();
	this->x_code =buffer.str();

	xcodeStream.close();
	delete OFD;
	return true;
}

bool Injector::SelectExecutable()
{
	if (!PE_Manager::SelectExecutable())
		return false;
	this->SubstitutionMap.insert(pair<DWORD, DWORD>(OEP, (DWORD)executable->get_ep()));
	return true;
}


void Injector::Inject(bool sectionTail)
{
	this->inOverlay = false;
	section EP_Section = this->executable->section_from_rva(this->executable->get_ep());
	if (sectionTail&&this->canAppentOverlayTo(EP_Section))
	{
		cout << "code section [" << EP_Section.get_name() << "] has enough place in overlay, using it" << endl;
		uint32_t offset = EP_Section.get_virtual_size();
		//EP_Section.set_virtual_size(offset + this->xcode_size);
		this->executable->set_section_virtual_size(EP_Section, offset + this->xcode_size);
		string &sectionData = EP_Section.get_raw_data();

		for (auto i = 0; i < xcode_size; i++)
			sectionData[offset + i] = this->x_code[i];

		for (auto i = 0; i < executable->get_number_of_sections(); i++)
			if (this->executable->get_image_sections()[i].get_name() == EP_Section.get_name())
			{
				this->executable->get_image_sections()[i] = EP_Section;
				break;
			}

		this->executable->set_ep(EP_Section.get_virtual_address() + offset);
		this->inOverlay = true;
	}
	else 
	{
		cout << "writing x code to a new section" << endl;
		section protectorSection;
		protectorSection.set_name(".prtctr");
		protectorSection.set_raw_data(x_code);
		protectorSection.set_characteristics(EP_Section.get_characteristics());
		//protectorSection.readable(true).writeable(true).executable(true);

		section &addedSection = this->executable->add_section(protectorSection);

		this->executable->set_section_virtual_size(addedSection, this->xcode_size);
		this->executable->set_ep(addedSection.get_virtual_address());
	}	
}

void Injector::AddImport(std::unordered_map<std::string, std::vector<std::string>>* importData)
{
	imported_functions_list imports(get_imported_functions(*this->executable));

	//�������� ����� ����������, �� ������� ����� ������������� �������

	for (const auto& lib : *importData)
	{
		
	}
	
}


bool Injector::SubstitutePatterns()
{
	DWORD* suspect=nullptr;
	unsigned short patternsNotFound = this->SubstitutionMap.size();
	for (auto i = 0; i < this->xcode_size - sizeof(DWORD); i++)
	{
		suspect = (DWORD*)(this->x_code.data() + i);
		//DWORD t = *suspect;
		auto substitution = this->SubstitutionMap.find(*suspect);
		if (substitution != this->SubstitutionMap.end())
		{
			cout << "mapping pattern: " << *suspect << " with " << substitution._Ptr->_Myval.second << endl;
			*(DWORD*)(this->x_code.c_str() + i) = substitution._Ptr->_Myval.second;//ReverseDWORD(substitution._Ptr->_Myval.second);
			patternsNotFound--;
		}
	}


	if (patternsNotFound != 0)
		cout << "smth went wrong" << endl;
	return patternsNotFound==0;
}



/// <summary>
/// �������� �� ������ ����� ���������
/// </summary>
/// <param name="s">������-��������</param>
/// <returns>true, ���� ����� ������� ��������. false �����</returns>
bool Injector::canAppentOverlayTo(pe_bliss::section &s)
{
	if (s.get_size_of_raw_data() - s.get_virtual_size() >= this->xcode_size && s.get_size_of_raw_data()-s.get_virtual_size() < this->executable->get_file_alignment())
	{
		ifstream tempStream(this->currentFilename, ios::binary|ios::in);
		if (tempStream.is_open())
		{
			std::stringstream buffer;
			buffer << tempStream.rdbuf();
			buffer.seekg(s.get_pointer_to_raw_data()+s.get_virtual_size());
			auto overlaySize = s.get_size_of_raw_data() - s.get_virtual_size();
			char* tempChar = new char[overlaySize];
			buffer.read(tempChar, overlaySize);

			for (auto i = 0; i < overlaySize;i++)
				if (tempChar[i] != '\0')
				{
					cout << "overlay contains non-null bytes, fuck that" << endl;
					return false;
				}
		}
	}
	else return false;

	cout << "overlay is fine" << endl;
	return true;
}
 
bool Injector::mapRequiredFunctions(char* dllName, unordered_map<string, DWORD> funcNames)
{
	bool dllFound, functionsFound = dllFound = false;
	auto funcsNotFound = funcNames.size();
	vector<imported_function> dllFunctions;
	string requestedLibName = string(dllName);

	transform(requestedLibName.begin(), requestedLibName.end(), requestedLibName.begin(), ::tolower);

	if (!this->executable->has_imports())
		return false;

	const imported_functions_list imports = get_imported_functions(*this->executable);
	for (const auto &LibIterator:imports)
	{
		string importedLibName = LibIterator.get_name();
		std::transform(importedLibName.begin(), importedLibName.end(), importedLibName.begin(), ::tolower);
		dllFound = false;
		if (importedLibName == requestedLibName)
		{
			DWORD FirstThunk = LibIterator.get_rva_to_iat();

			dllFound = true;
			dllFunctions.clear();
			dllFunctions.swap(vector<imported_function>());
			dllFunctions = LibIterator.get_imported_functions();

			for (auto i = 0; i < dllFunctions.size(); i++)
			{
				auto funcIterator = funcNames.find(dllFunctions[i].get_name());
				if (funcIterator == funcNames.end())
					continue;

				funcsNotFound--;
				SubstitutionMap.insert(pair<DWORD, DWORD>(funcIterator._Ptr->_Myval.second, (DWORD)(FirstThunk+ i*sizeof(IMAGE_THUNK_DATA32))));
				funcNames.erase(funcIterator._Ptr->_Myval.first);
			}
		}
	}

	if (funcsNotFound > 0)
		for (const auto& lostFuncs : funcNames)
			cout << "didn't manage to find " << dllName << "::" << lostFuncs.first << endl;

	return dllFound&&funcsNotFound==0;
}

#pragma region hide



//bool Injector::Encrypt(CryptoAlgorithm CA, DWORD key)
//{
//
//	if (!this->executable)
//		return false;
//	pe_bliss::section_list sections = this->executable->get_image_sections();
//	for (auto i = 0; i<3; i++)
//		if ((sections[i].get_characteristics()&IMAGE_SCN_MEM_EXECUTE) > 0)
//		{
//
//			//section temp = sections[i];
//			if (!inOverlay)
//				sections[i].get_raw_data() = CA(sections[i].get_raw_data(), &key);
//			else sections[i].get_raw_data() = CA(sections[i].get_raw_data().substr(0, sections[i].get_virtual_size() - this->xcode_size), &key)
//											+sections[i].get_raw_data().substr(sections[i].get_virtual_size() - this->xcode_size,this->xcode_size);
//			this->executable->get_image_sections()[i] = sections[i];// = temp;
//		}
//	return true;
//}
//
//std::string& Algorithms::CaesarCypher(std::string& s, void *params)
//{
//	DWORD key = *((DWORD*)params);
//	for (int i = 0; i < s.size(); i++)
//		s[i] = s[i]+key;
//	return s;
//}
//
//std::string & Algorithms::AES(std::string & s, void *param)
//{
//	string rezult;
//	byte key[CryptoPP::AES::MAX_KEYLENGTH], iv[CryptoPP::AES::MAX_KEYLENGTH];
//	memset(key, 0x00, CryptoPP::AES::MAX_KEYLENGTH);
//	SHA256 sha256;
//	RandomNumberGenerator RNG;
//	
//	byte randomLength =16+ (RNG.GenerateByte()%49);
//	byte* magic = new byte[randomLength];
//	for (auto i = 0; i < randomLength; i++)
//		magic[i] = RNG.GenerateByte();
//
//	sha256.CalculateDigest(key, magic, randomLength);
//	memset(iv, 0x00, CryptoPP::AES::BLOCKSIZE);
//
//	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::MAX_KEYLENGTH);
//	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);
//
//	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(rezult));
//	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(s.c_str()), s.length() + 1);
//	stfEncryptor.MessageEnd();
//
//	std::cout << "Cipher Text (" << rezult.size() << " bytes)" << std::endl;
//
//	for (int i = 0; i < rezult.size(); i++)
//		std::cout << "0x" << std::hex << (0xFF & static_cast<byte>(rezult[i])) << " ";
//
//	std::cout << std::endl << std::endl;
//
//	//CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
//	//CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);
//
//	//CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedtext));
//	//stfDecryptor.Put(reinterpret_cast<const unsigned char*>(ciphertext.c_str()), ciphertext.size());
//	//stfDecryptor.MessageEnd();
//
//	////
//	//// Dump Decrypted Text
//	////
//	//std::cout << "Decrypted Text: " << std::endl;
//	//std::cout << decryptedtext;
//	//std::cout << std::endl << std::endl;
//
//	return s;
//}
//
//std::string &Algorithms::Salsa20(std::string& s, void* param)
//{
//	AutoSeededRandomPool prng;
//
//	string ciphertextStr("");
//	byte *plaintextBytes = (byte*)s.data();
//	//I could statically allocate this, but then changes will require work, and typing
//	byte *ciphertextBytes = new byte[s.length()];
//
//	//~Key and IV Generation/Initialization======================================
//	byte key[32];
//	byte iv[8];
//	prng.GenerateBlock(key, 32);
//	prng.GenerateBlock(iv, 8);
//
//	//~Encryption================================================================
//	Salsa20::Encryption salsa;
//	salsa.SetKeyWithIV(key, 32, iv);
//	salsa.ProcessData(ciphertextBytes, plaintextBytes, s.length());
//	ciphertextStr.assign((char*)ciphertextBytes);
//	cout << "MASTER KEY FOR TEXT SECTION: ";
//	for (const byte &x : key)
//		cout << ((int)x & 0xFF) << " ";
//	cout << endl;
//
//	cout << "IV FOR MASTER KEY: ";
//		for (const byte &x : iv)
//			cout << ((int)x & 0xFF) << " ";
//	cout << endl;
//
//	//Reset Key & IV
//	//!!! THIS IS IMPORTANT: If you do not reset the stream cipher the data will
//	//be encrypted again with a different part of the streaming key
//	//Resetting the key & IV ensure that the same key is used, and we decrypt
//	/////////////////////////////////////////////////////////////////////////////
//	/////////////////////////////////////////////////////////////////////////////
//	s = ciphertextStr;
//	return s;
//}
#pragma endregion