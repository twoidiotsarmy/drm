#include "precompiled.h"
#include "Food.h"
#include "Misc.h"
#include "Game_Snake.h"


#ifndef ___SNAKEE_
#define ___SNAKEE_


class Snake {
public:
	void Eat(Food*);
	bool Move();
	~Snake();
	Snake(coordinate origins);
	//inline Snake* operator += (Food *);
	//inline Snake* operator + (Food&);
	const Direction & get_direction();
	void set_direction(Direction);
	const coordinate* Head();
private:
	bool movePending;
	WORD length;
	wchar_t head;
	const wchar_t body = '@';
	Direction currentDirection;
	std::vector<coordinate> _coordinates;
	short _undigested;
	bool isOverlapping();
	friend class Game_Snake;
};

#endif // !___SNAKEE_
