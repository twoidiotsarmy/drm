#include "precompiled.h"
#define X64
#pragma optimize("", off)
#pragma code_seg(".text")

typedef HMODULE(_stdcall *LoadLibraryDelegate)(char*);
typedef FARPROC(_stdcall *GetProcAddressDelegate)(HMODULE, char*);
typedef BOOL(__stdcall *FreeLibraryDelegate)(HMODULE);
#ifdef X64
typedef int(__stdcall *VerifyDelegate)(DWORD64*);
#endif // X64
#ifdef X86
typedef int(__stdcall *VerifyDelegate)(DWORD*);
#endif // X86



typedef void(__stdcall *CALL_OEP)();

#pragma region hide
#ifdef X86
void __declspec(naked) X_CODE()
{
	__asm
	{
		push ebp
		mov ebp, esp
		sub esp, 64
	}
	DWORD IMAGE_BASE;//4
	DWORD OEP; OEP = 0xDEADC0D0;//8
	DWORD* LoadLibraryAddress; LoadLibraryAddress = (DWORD*)0xDEADC0D1;//12
	DWORD* GetProcAddressAddress; GetProcAddressAddress = (DWORD*)0xDEADC0D2;//16
	//DWORD* FreeLibraryAddress; FreeLibraryAddress = (DWORD*)0xDEADC0D3;//20
	char buffer[8];//28
	LoadLibraryDelegate LoadLibraryInvoker;//32
	HMODULE lib0;//36
	GetProcAddressDelegate GetProcAddressInvoker;//40
	VerifyDelegate Verifier;//44
	//FreeLibraryDelegate FreeLibraryInvoker; //48

	__asm{
		CALL NEXT
		next:
		POP EAX
		and eax, 0FFFF0000h
		search :
		cmp word ptr[eax], 0x5a4d
			je stop
			sub eax, 00010000h
			jmp search
			stop :
			mov[IMAGE_BASE], eax
	}


	//OEP += IMAGE_BASE;


	LoadLibraryInvoker = (LoadLibraryDelegate)(*(DWORD*)(IMAGE_BASE + (DWORD)LoadLibraryAddress));

	*(DWORD*)(&buffer[0]) = 'trP';// = 'resu';
	*(DWORD*)(&buffer[4]) = 'lld.';// = 'd.23';

	lib0 = LoadLibraryInvoker(buffer);

	GetProcAddressInvoker = (GetProcAddressDelegate)(*(DWORD*)(IMAGE_BASE + (DWORD)GetProcAddressAddress));

	*(DWORD*)(&buffer[0]) = '\0frV';

	Verifier = (VerifyDelegate)GetProcAddressInvoker(lib0, buffer);

	Verifier(&OEP);
	__asm 
	{
		MOV ESP, EBP
		POP EDX
		XOR EDX, EDX
		JMP [OEP]
	}
}

#endif // X86

#ifdef X64


void X_CODEx64()
{
	DWORD64 IMAGE_BASE;//4
	DWORD64 OEP = 0xDEADC0D0DEADC0D0;//8
	DWORD64* LoadLibraryAddress = (DWORD64*)0xDEADC0D1DEADC0D1;//12
	DWORD64* GetProcAddressAddress = (DWORD64*)0xDEADC0D2DEADC0D2;//16
	char buffer[8];//28
	LoadLibraryDelegate LoadLibraryInvoker;//32
	HMODULE lib0;//36
	GetProcAddressDelegate GetProcAddressInvoker;//40
	VerifyDelegate Verifier;//44
	DWORD64 *VA = (DWORD64*)((DWORD64)&X_CODEx64 & 0xFFFF);
	while (*VA != 0x5A4D)
		VA -= 0x10000;
	IMAGE_BASE = (DWORD64)VA;

	OEP += IMAGE_BASE;

	LoadLibraryInvoker = (LoadLibraryDelegate)(*(DWORD64*)(IMAGE_BASE + (DWORD64)LoadLibraryAddress));

	*(DWORD*)(&buffer[0]) = '.trP';// = 'resu';
	*(DWORD*)(&buffer[4]) = '\0lld';// = 'd.23';

	lib0 = LoadLibraryInvoker(buffer);

	GetProcAddressInvoker = (GetProcAddressDelegate)(*(DWORD64*)(IMAGE_BASE + (DWORD64)GetProcAddressAddress));

	*(DWORD*)(&buffer[0]) = '\0frV';

	Verifier = (VerifyDelegate)GetProcAddressInvoker(lib0, buffer);
	CALL_OEP CO = (CALL_OEP)Verifier(&OEP);
	CO();
}
#pragma endregion
#endif // X64


//void __declspec(naked) X_EDGE() { }
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{

	//X_CODEx64();
#ifdef X86
	X_CODE();
#else 
	X_CODEx64();
#endif // X86
	return 0;
}
#pragma optimize("", on)
