#pragma once
#include "PE_Manager.h"
typedef unsigned char byte;
typedef std::string (*CryptoAlgorithm)(std::string& s, const byte const *);

class Cryptor:public PE_Manager {
public:
	Cryptor();
	~Cryptor();

	bool Encrypt(CryptoAlgorithm CA, const byte const * const key, const bool& inOverlay, const int& padding );

	const byte * const GenerateKey();

	//void borrowExecutable(PE_Manager&);
private:
	pe_bliss::pe_base *executable;
	wchar_t* currentFilename;
};

namespace Algorithms 
{
	std::string& CaesarCypher(std::string& s, const byte const * key);
	std::string& AES(std::string& s, const byte const * key);
	std::string Salsa20(std::string&, const byte const * key);
}	