#include "precompiled.h"


#include "soundtrack.h"
#include "Game_Snake.h"


DWORD  __stdcall gameThreadFunction(void* subject);
long __stdcall hook_proc(int code, unsigned int wParam, long lParam);
DWORD  keyboardMessageLoop();

HHOOK KeyboardHook;
theGame *the_game;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	setlocale(LC_ALL, "Russian");
	the_game = new Game_Snake(2,GetCurrentThreadId());
	//LPDWORD threadID = nullptr;
	the_game->Launch();
	void* gameThread = CreateThread(NULL,0,&gameThreadFunction, the_game, 0,NULL);

	if (KeyboardHook = SetWindowsHookEx(WH_KEYBOARD_LL, &hook_proc, NULL, NULL))
		keyboardMessageLoop();

	delete the_game;

	return 0;// keyboardMessageLoop(nullptr);
}
long __stdcall hook_proc(int code, unsigned int wParam, long lParam)
{
	if (wParam == WM_KEYDOWN)
	{
		KBDLLHOOKSTRUCT *KbdInfo = reinterpret_cast<KBDLLHOOKSTRUCT*>(lParam);

		switch (KbdInfo->vkCode)
		{
		case VK_UP:
		case VK_RIGHT:
		case VK_DOWN:
		case VK_LEFT:
			if (the_game->OnAir())
				the_game->Input_SetDirection(static_cast<Direction>(KbdInfo->vkCode));
			break; 
		case VK_SPACE:
			the_game->TogglePause();
			
			break;
		case VK_ESCAPE:
			the_game->Exit();
			break;
		default:
			break;
		}
	}

	return CallNextHookEx(KeyboardHook, code, wParam, lParam);
}


//AM NOT EVEN SURE IF I NEED THIS
DWORD  keyboardMessageLoop()
   {
	MSG msg;
	BOOL bRet;

	while ((bRet= GetMessage(&msg, NULL, 0, 0)) > 0)
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);

	}
	UnhookWindowsHookEx(KeyboardHook);
	return msg.wParam;
}

DWORD  __stdcall gameThreadFunction(void * subject)
{
	Game_Snake *game = static_cast<Game_Snake*>(subject);
	game->hThread_GameLoop = GetCurrentThread();
	while (game->OnAir())
		game->GameLoop();
	return 0;
}
