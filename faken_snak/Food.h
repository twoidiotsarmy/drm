#include "precompiled.h"
#include "Misc.h"
#ifndef __FOODOO
class Food {
public:
	Food(coordinate&, WORD, void*);
	~Food();
	const coordinate & get_location();
	const WORD & get_weight();
	const wchar_t Sign = '$';

	//inline operator const COORD();
private:
	WORD weight;
	coordinate location;
	
	void* hEvent;
};
#define __FOODOO
#endif // !__FOODOO
